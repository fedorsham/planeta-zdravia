var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    path = require('path'),
    autoprefixer = require('autoprefixer-core'),
    postcss = require('gulp-postcss');

var params = {
  out: 'public',
  htmlSrc: 'index-raw.html',
  levels: ['blocks']
},
    getFileNames = require('html2bl').getFileNames(params);

gulp.task('default', ['build']);

gulp.task('server', function() {
  browserSync.init({
    server: params.out
  });

  gulp.watch('*.html', ['html']);
  gulp.watch(params.levels.map(function(level) {
    var cssGlob = level + '/**/*.css';
    return cssGlob;
  }), ['css']);
  gulp.watch(params.levels.map(function(level) {
    var jsGlob = level + '/*.js';
    return jsGlob;
  }), ['js']);
  gulp.watch(params.levels.map(function(level) {
    var imgGlob = level + '/**/*.{jpg,png,svg,ico}';
    return imgGlob;
  }), ['images']);
  gulp.watch(params.levels.map(function(level) {
    var fntGlob = level + '/**/*.{eot,woff,ttf}';
    return fntGlob;
  }), ['fonts']);
});

gulp.task('build', ['html', 'css', 'fonts', 'images', 'js']);

gulp.task('html', function() {
  gulp.src(params.htmlSrc)
      .pipe(rename('index.html'))
      .pipe(gulp.dest(params.out))
      .pipe(browserSync.stream());
});

gulp.task('css', function() {
  getFileNames.then(function(files) {
    console.log(files.css);
    gulp.src(files.css)
        .pipe(concat('styles.css'))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest(params.out))
        .pipe(browserSync.stream());
  })
              .done();
});

gulp.task('images', function() {
  getFileNames.then(function(source) {
    gulp.src(source.dirs.map(function(dir) {
      var imgGlob = path.resolve(dir) + '/*.{jpg,png,svg,ico}';
      return imgGlob;
    }))
        .pipe(gulp.dest(params.out));
  })
  .done();
});

gulp.task('fonts', function() {
  getFileNames.then(function(source) {
    gulp.src(source.dirs.map(function(dir) {
      var fontGlob = path.resolve(dir) + '/*.{eot,woff,ttf}';
      return fontGlob;
    }))
        .pipe(gulp.dest(params.out));
  })
              .done();
});

gulp.task('js', function() {
  getFileNames.then(function(source) {
    gulp.src(source.dirs.map(function(dir) {
      var jsGlob = path.resolve(dir) + '/*.js';
      return jsGlob;
    }))
        .pipe(gulp.dest(params.out))
        .pipe(browserSync.stream());
  })
              .done();
});
