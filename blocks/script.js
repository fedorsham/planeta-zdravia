var images = document.querySelectorAll(".gallery__image");
var container_left = document.querySelector(".gallery__image-container_left");
var container_middle = document.querySelector(".gallery__image-container_middle");
var container_right = document.querySelector(".gallery__image-container_right");
var product_lists = document.querySelectorAll(".product-grid");
var contentArray = document.querySelectorAll(".review__content");
var authorArray = document.querySelectorAll(".review__author");

var indexes = [];
var index;

function showProducts(e) {
  product_lists.forEach(function(item) {
    item.style.display="none"; });
  document.querySelector(e.getAttribute("href")).style.display="flex";
  document.querySelectorAll(".item-link_red").forEach(function(item) {
    item.style.background="#fff";
    item.style.color="#891c07";
  });
  e.style.background="#891c07";
  e.style.color="#fff";
}

function showGalleryInit() {
  for (var i = 0; i < images.length; i++) {
    if (i >= 3) { break; }
    indexes[i] = i;
  }
  index = showGallery(indexes);
}
showGalleryInit();

function rotateImage(n) {
  if (images.length == 0) return;
  if (images.length < 3) {
    if (images.length == 2) {
      var tmp = indexes[0];
      indexes[0] = indexes[1];
      indexes[1] = tmp;
    }
    index = showGallery(indexes);
    return;
  }
  index += n;
  switch (index) {
    case -1:
    case (images.length - 1):
      indexes[0] = images.length - 2;
      indexes[1] = images.length - 1;
      indexes[2] = 0;
      break;
    case 0:
    case (images.length):
      indexes[0] = images.length - 1;
      indexes[1] = 0;
      indexes[2] = 1;
      break;
    default:
      indexes[0] = index - 1;
      indexes[1] = index;
      indexes[2] = index + 1;
  }
  index = showGallery(indexes);
}

function removeChildren(node) {
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

function showGallery() {
  if (images.length == 0) return;
  removeChildren(container_left);
  removeChildren(container_middle);
  removeChildren(container_right);
  for (var i = 0; i < images.length; i++) {
    images[i].style.display="none";
  }
  for (var i = 0; i < indexes.length; i++) {
    images[indexes[i]].style.display="block";
    switch(i) {
      case 0:
        if (images.length == 1) {
          container_middle.appendChild(images[indexes[i]]);
          return 0;
        }
        container_left.appendChild(images[indexes[i]]);
        break;
      case 1:
        container_middle.appendChild(images[indexes[i]]);
        break;
      case 2:
        container_right.appendChild(images[indexes[i]]);
    }
  }
  return indexes[1];
}

function showText(e) {
  var c = e.childNodes;
  c[3].style.visibility="visible";
  c[5].style.visibility="hidden";
  c[7].style.visibility="visible";
}

function showImage(e) {
  var c = e.childNodes;
  c[3].style.visibility="hidden";
  c[5].style.visibility="visible";
  c[7].style.visibility="hidden";
}

var reviewIndex = 1;
function addToReviewIndex(n) {
  showReview(reviewIndex += n);
}

function showReview(n) {
  var i;
  if (n > contentArray.length) {reviewIndex = 1}
  if (n < 1) {reviewIndex = contentArray.length}
  for (i = 0; i < contentArray.length; i++) {
    contentArray[i].style.display = "none";
    authorArray[i].style.display = "none";
  }
  contentArray[reviewIndex-1].style.display = "block";
  authorArray[reviewIndex-1].style.display = "block";
}
